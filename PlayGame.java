import java.util.Scanner;

public class PlayGame {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        Conditions condition = new Conditions();
        Rules rules = new Rules();
        Config config = new Config();
        boolean gameyGame = true;

        System.out.println("Welcome to Roshambo:");
        System.out.println("\nTo view rules press R.\nTo view game type press G.");

        if (scan.nextLine().toUpperCase().equals("R")) {
           rules.RulesReader();
        }
        else if(scan.nextLine().toUpperCase().equals("G")) {
            config.ConfigReader();
        }

            while (gameyGame) {
                condition.start(config);

                System.out.println("Press any key to play again...");
                System.out.println("(Tired of losing??? Press 'Q' to quit)");

                if (scan.next().toUpperCase().equals("Q")) {
                    gameyGame = false;
                }

            }
            condition.showStats();
        }
}
