import java.util.Scanner;

public class Player extends GameMethods{

    private Scanner scan;

    public Player(){
        scan = new Scanner(System.in);
    }

    public Plays getPlay(){
        System.out.println("\nPlease enter your choice:");
        String playerChoice = scan.nextLine().toUpperCase();

        switch(playerChoice) {
            case "R":
                return Plays.ROCK;
            case "P":
                return Plays.PAPER;
            case "S":
                return Plays.SCISSORS;
            case "L":
                return Plays.LIZARD;
            case "K":
                return Plays.SPOCK;
        }

        System.out.println("Please enter a valid input...");
        return getPlay();
    }
}
