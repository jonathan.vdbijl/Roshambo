import java.io.*;
import java.util.Properties;

public class Config{

    private File gametype = new File("Config.txt");
    private Properties props = new Properties();
    public String Game = "";
    public int value;

    public void ConfigReader() {

        try {
            BufferedReader reader2 = new BufferedReader(new FileReader(gametype));
            props.load(reader2);
            Game = props.getProperty("Game");

            if (Game.equals("1")) {
                System.out.print("\nYou are playing Normal Roshambo: ");
                System.out.println("P = Paper, S = Scissors, R = Rock");
                this.value = 3;
            } else if (Game.equals("2")) {
                System.out.print("You are playing Big Bang Roshambo: ");
                System.out.println("P = Paper, S = Scissors, R = Rock, L = Lizard, K = Spock");
                this.value =5;
            }
            reader2.close();

        } catch (FileNotFoundException e) {
            System.out.println("file not found");
        } catch (IOException e) {
            System.out.println("cannot read file");
        }

    }
    public int bound() {
        return value;
    }
}
