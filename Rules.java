import java.io.*;

public class Rules{

    private File rules = new File("Rules.txt");

    public void RulesReader(){

        try{
            BufferedReader reader1 = new BufferedReader(new FileReader(rules));
            String rulesline;
            while ((rulesline = reader1.readLine()) != null) {
                System.out.println(rulesline);
            }
            reader1.close();

        } catch (FileNotFoundException e) {
            System.out.println("file not found");
        } catch (IOException e) {
            System.out.println("cannot read file");
        }
        }
}
