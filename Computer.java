import java.util.Random;

public class Computer extends GameMethods {
   private Random generator;

    public Computer(){
        generator = new Random();
    }

    public Plays getPlay(Config config){
        int pcout = generator.nextInt(config.bound()) + 1;

        switch (pcout) {
            case 1:
                return Plays.ROCK;
            case 2:
                return Plays.PAPER;
            case 3:
                return Plays.SCISSORS;
            case 4:
                return Plays.LIZARD;
        }
            return Plays.SPOCK;
        }
}

