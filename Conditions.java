public class Conditions extends GameMethods {

    private Player player;
    private Computer computer;
    private Plays playerChoice;
    private Plays computerChoice;
    private Results result;
    private static int win;
    private static int lose;
    private static int tie;

    public Conditions(){
        player = new Player();
        computer = new Computer();
    }

    private Results getResult(){
        if (playerChoice == computerChoice)
            return Results.TIE;

        switch (playerChoice) {
            case ROCK:
                return (computerChoice == Plays.SCISSORS || computerChoice == Plays.LIZARD ? Results.WIN: Results.LOSE);
            case PAPER:
                return (computerChoice == Plays.ROCK || computerChoice == Plays.SPOCK ? Results.WIN : Results.LOSE);
            case SCISSORS:
                return (computerChoice == Plays.PAPER || computerChoice == Plays.LIZARD ? Results.WIN : Results.LOSE);
            case LIZARD:
                return (computerChoice == Plays.PAPER || computerChoice == Plays.SPOCK ? Results.WIN : Results.LOSE);
            case SPOCK:
                return (computerChoice == Plays.ROCK || computerChoice == Plays.LIZARD ? Results.WIN : Results.LOSE);

        }
        return Results.LOSE;
    }

    private void showResults(){
        switch (result){
            case WIN:
                System.out.println(playerChoice + " beats " + computerChoice + ", YOU WIN!!!");
                break;
            case LOSE:
                System.out.println(playerChoice + " loses to " + computerChoice + ", YOU LOSE!!!");
                break;
            case TIE:
                System.out.println("It's a TIE!!!");
                break;
        }
    }

    private void Stats(){
        if(result == Results.WIN)
            win++;
        else if(result == Results.LOSE)
            lose++;
        else
            tie++;
    }

    public void showStats(){
        System.out.println("You have played: " + (win + lose + tie) + " games.");
        System.out.println("You have won: " + win + " games.");
        System.out.println("You have lost: " + lose + " games.");
        System.out.println("You have tied: " + tie + " games.");
        System.out.println("Thank you for playing!");
    }

    public void start(Config config){
        playerChoice = player.getPlay();
        computerChoice = computer.getPlay(config);
        System.out.println("Computer played: " + computerChoice);
        result = getResult();
        showResults();
        Stats();
    }

}

